import Head from 'next/head'

export default function Home() {
  return (
    <div className="container">
      <Head>
        <title>Ehsan Khakian</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <header>
        <div>
          <div>
            <h1>Ehsan Khakian</h1>
            <strong>Full Stack Web Developer</strong>
          </div>
        </div>
      </header>
      <main>
        <article>
          <h2>Experiences</h2>
          <ul>
            <li>Exchange Magix (2024)</li>
            <p>Developed and maintained a cryptocurrency and fiat exchange with micro-service architecture. (NestJS / ReactJS).</p>
            <li>Liateam (2020-2024)</li>
            <p>Led the front-end team in delivering high-quality products such as e-commerce platforms, Learning Management Systems (LMS), administrative panels, and AI interfaces. (Nextjs / ReactJS / Express)</p>
            <li>Iran-UAE Chamber of Commerce (2019)</li>
            <p>Developed and maintained the main website and landing pages using a custom CMS. (Express and Ember.js)</p>
            <li>Freelance (2018)</li>
            <p>Developed and refactored multiple websites and web applications for various businesses</p>
          </ul>
          <h2>Some Links</h2>
          <ul>
            <li><a href="https://exchangemagix.com/">Exchange Magix</a></li>
            <li><a href="https://liateam.com/">Liateam</a></li>
            <li><a href="https://iremcc.ir/">Iran-UAE Chamber of Commerce</a></li>
            <li><a href="https://ezp.co.ir/">EZP Co.</a></li>
            <li><a href="https://acmai.org/">Association of Catalyst Producers of Iran</a></li>
          </ul>
          <h2>Education</h2>
          <ul>
            <li>B.S. in Power Electrical Engineering from Lorestan University</li>
          </ul>
        </article>
        <aside>
          <h2>Front-end</h2>
          <ul>
            <li>React</li>
            <li>NextJS</li>
            <li>React Query / Zustand</li>
            <li>PWA / Service Worker</li>
            <li>Jest / Enzyme / Cypress</li>
            <li>Babel / Webpack / Vite</li>
          </ul>
          <h2>Back-end</h2>
          <ul>
            <li>NodeJS</li>
            <li>NestJS</li>
            <li>Express</li>
            <li>MongoDB, MySQL</li>
            <li>RabbitMQ</li>
          </ul>
          <h2>Familiar With</h2>
            <ul>
              <li>ReactNative</li>
              <li>Assembly / C</li>
              <li>UWP / C#</li>
            </ul>
        </aside>
      </main>
      

      <footer>
        <div>
          <div className="social">
            <p>phone: <a href="tel:+989050391817">+989050391817</a></p>
            <p>github: <a href="https://github.com/3hson">github.com/3hson</a></p>
            <p>telegram: <a href="https://t.me/ehsankhakian">t.me/ehsankhakian</a></p>
            <p>site: <a href="https://ehsankhakian.ir">ehsankhakian.ir</a></p>
          </div>
          <a
            className="credit"
            href="https://nextjs.org"
            target="_blank"
            rel="noopener noreferrer"
          >
            <img src="/nextjs.svg" alt="NextJS Logo" className="logo" />
          </a>
        </div>
      </footer>

      <style jsx>{`
        .container {
          min-height: 100vh;
          padding: 0 0.5rem;
          display: flex;
          flex-direction: column;
          justify-content: center;
          align-items: center;
          font-size: 18px;
          font-weight: 600;
        }
        header {
          width: 100%;
        }
        header > div {
          display: flex;
          justify-content: space-between;
          border-bottom: 2px solid #eaeaea;
          margin: 2rem 4rem 0;
          padding-bottom: 1rem;
        }
        header h1 {
          margin: 0
        }
        main {
          flex-direction: row;
          display: flex;
          margin: 0;
          width: 100%;
          min-height: 45rem

        }
        article {
          flex: 1 calc(65% - 6rem);
          margin: 0rem 2rem 0 4rem;
          border-bottom: 2px solid #eaeaea;
        }
        aside {
          flex: 1 calc(35% - 5rem);
          padding: 0rem 4rem 1rem 0rem;
          padding-left: 1rem;
          border-left: 2px solid #eaeaea;
        }
        ul {
          list-style-type: none;
          padding-left: 0;
        }
        ul > li:before {
          content: "-";
          padding-right: 0.5rem;
          text-indent: -0.5rem; 
        }
        ul > p {
          margin-top: 0.5rem;
        }
        footer {
          width: 100%;
          justify-content: space-between;
          align-items: center;
        }
        footer > div {
          margin: 1rem 4rem 2rem;
          display: flex;
          justify-content: space-between;
        }
        footer .credit {
          display: flex;
          align-items: center;
          height: min-content;
          align-self: flex-end;
        }

        .social {
          display: flex;
          flex-direction: column;
        }
        .social p {
          margin: 0;
        }
        .social a {
          display: inline-block;
        }
        .logo {
          height: 1.5em;
          margin-left: 0;
        }
        @media (max-width: 960px) {
          .container {
            font-size: 16px;
            font-weight: 600;
          }
          header > div {
            margin: 1rem 2rem 0;
          }
          article {
            margin: 0rem 2rem 0 2rem;
          }
          aside {
            padding: 0rem 1rem 1rem 0rem;
            padding-left: 1rem;
          }
          footer > div {
            margin: 2rem;
          }
        }
        @media (max-width: 600px) {
          header > div {
            margin: 2rem 1rem 0;
          }
          main {
            flex-direction: column;
          }
          article {
            margin: 0rem 1rem 0 1rem;
          }
          aside {
            margin: 0 1rem;
            padding: 0;
            border-left: none;
            border-bottom: 2px solid #eaeaea;
          }
          footer > div {
            margin: 1rem 1rem 2rem;
            display: flex;
            flex-direction: column;
            justify-content: space-between;
          }
          footer .credit {
            align-self: center;
            padding-top: 1rem
          }
        }
      `}</style>

      <style jsx global>{`
        html,
        body {
          padding: 0;
          margin: 0;
          color: #707070;
          font-family: -apple-system, BlinkMacSystemFont, Segoe UI, Roboto,
            Oxygen, Ubuntu, Cantarell, Fira Sans, Droid Sans, Helvetica Neue,
            sans-serif;

        }

        a {
          text-decoration: none;
        }

        a:visited {
          color: blue;
        }
        
        * {
          box-sizing: border-box;
        }

      `}</style>
    </div>
  )
}
